<?php

namespace twofox\news\models;

use common\models\User;
use DOMDocument;
use twofox\news\Module;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\Expression;
use yii\helpers\FileHelper;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $annonce
 * @property string $content
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $user_id
 *
 * @property User $user
 */
class News extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $imageGetUrl;
    public $imageUploadPath;
    public $uploadTempPath;
    public $imageGetTempUrl;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_NOT_ACTIVE => Module::t('twofox-news', 'NOT ACTIVE'),
            self::STATUS_ACTIVE => Module::t('twofox-news', 'ACTIVE')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['annonce'], 'string', 'max' => 1000],
            [['content'], 'string', 'max' => 30000],
            [['status'], 'in', 'range' => array_keys(self::getStatusArray())],
            [['status'], 'default', 'value' => self::STATUS_NOT_ACTIVE],
            [['created_at', 'updated_at', 'user_id'], 'safe'],
            [['title', 'slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('twofox-news', 'ID'),
            'title' => Module::t('twofox-news', 'Title'),
            'annonce' => Module::t('twofox-news', 'Annonce'),
            'content' => Module::t('twofox-news', 'Content'),
            'published' => Module::t('twofox-news','PUBLISHED'),
            'meta_title' => Module::t('twofox-news','META TITLE'),
            'meta_keywords' => Module::t('twofox-news','META KEYWORDS'),
            'meta_description' => Module::t('twofox-news','META KEYWORDS'),
            'status' => Module::t('twofox-news', 'Status'),
            'created_at' => Module::t('twofox-news', 'Created At'),
            'updated_at' => Module::t('twofox-news', 'Updated At'),
            'user_id' => Module::t('twofox-news', 'Author'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Yii::$app->controller->module->authorClass, ['id' => 'user_id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            if(!Yii::$app->user->id){
                return false;
            }
            $this->user_id = Yii::$app->user->id;
        }
        
        if (!$this->annonce || !$this->content) {
            return false;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            //когда узнали id обновляем данные, beforeSave вызовет $this->imageShift();
            $this->save();
        }

    }
    
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getTranslations(){
        return $this->hasMany(NewsTranslation::className(), ['post_id' => 'id'])->where(['class' => self::className()]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression("'" . date('Y-m-d H:i:s') . "'"),
            ],            
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
                'uniqueValidator' => []
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['annonce', 'content', 'meta_title', 'meta_keywords', 'meta_description'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ]
        ];
    }
    
    

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors)." ";
        }
        return $result;
    }
}
